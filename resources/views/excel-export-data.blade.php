<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <h1 class="bg-success text-center m-3 p-3 text-white">Import Data From Excel File Using Laravel</h1>


    <div class="container-fluid">
      
    
      <div class="row">
        <div class="col-sm-12 text-center">
            <form action="{{url('/export-data')}}" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  @csrf
                    <input type="submit" class="btn btn-primary" value="Export Data">
                </div>
            </form>
            {{-- https://docs.laravel-excel.com/2.1/export/array.html --}}
            {{-- https://github.com/fzaninotto/Faker#fakerprovideren_uscompany --}}
            {{-- https://www.facebook.com/groups/2236047909778502/ --}}
            {{-- https://gitlab.com/eraasoft/how-to-export-data-from-database-to-excel-file-using-laravel --}}
        </div>
    </div>
        @if(isset($results))
        <div class="row">
          <div class="col-sm-12">

              <table class="table table-bordered border ">
                <thead>
                  <tr>
                    <th> Company Name </th>
                    <th> Email</th>
                    <th> Address </th>
                    <th> Mobile </th>
                    <th> Phone </th>
                  </tr>
                </thead>

                <tbody>
                  @foreach($results as $row)
                  <tr>
                    <td>{{$row->name}}</td>
                    <td>{{$row->email}}</td>
                    <td>{{$row->address}}</td>
                    <td>{{$row->mobile}}</td>
                    <td>{{$row->phone}}</td>
                  </tr>
                  @endforeach

                </tbody>
                

              </table>

          </div>
        </div>
        @endif
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" ></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
  </body>
</html>